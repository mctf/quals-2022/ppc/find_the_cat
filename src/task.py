#!/usr/bin/python3

import os
import random
from time import time


def fail(try_n, t):
    if try_n == 2:
        print("Oh, too many tries, cat ran away :(")
        exit(0)

    print(
        f"Oh, our cat isn`t here, but cat run to specified address around {t} minutes")


def fail_ValueError():
    print("Is it address? I think no.")
    exit(0)


def fail_time():
    print("Too slow, cat ran away :(")
    exit(0)


def right():
    print(os.environ["FLAG"])
    exit(0)


def check(ans, cat_x, try_n):
    try:
        ans = int(ans)
    except ValueError:
        fail_ValueError()

    if ((ans < -1000) or (ans > 1000)):
        fail_ValueError()

    if cat_x == ans:
        right()
    else:
        fail(try_n, int(abs(cat_x - ans)/CAT_SPEED))


ITERATIONS_NUM = 3
TIMEOUT_SEC = 2
CAT_SPEED = random.randint(2, 5) * 2


def main():
    cat_x = random.randint(-1000, 1000)

    print("You have 3 tries to find the cat")

    for i in range(ITERATIONS_NUM):
        start = time()
        ans = input("Where is the cat???\n")

        if time() - start > TIMEOUT_SEC:
            fail_time()

        check(ans, cat_x, i)


if __name__ == '__main__':
    main()
